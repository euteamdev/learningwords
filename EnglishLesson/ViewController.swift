//
//  ViewController.swift
//  EnglishLesson
//
//  Created by evgen on 16.03.19.
//  Copyright © 2019 evgen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelOverTable: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelOverTable.text = Setting.shared.textLabelOverTable
        labelOverTable.backgroundColor = Setting.shared.colorLabelOverTable
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        tableView.reloadData()
    }
    
    //Переход на окно со словами
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.destination is MessageIsReadyViewController else { fatalError("error") }
        levelsId = Int((tableView.indexPathForSelectedRow?.row)!)
    }
}

//MARK: - extension for table
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Levels.shared.levelsArray.levels.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CellLevel
        
        let dataCell = Levels.shared.levelsArray.levels[indexPath.row]
        
        cell.blockNameLabel.text = dataCell.blockName
        cell.descLabel.text = dataCell.desc
        cell.isDone = dataCell.isDone
        cell.isOpen = dataCell.isOpen
        
       
        if dataCell.isDone {
            cell.imageViewLb.image = #imageLiteral(resourceName: "DoneBlockImg")
        } else {
            cell.imageViewLb.image = #imageLiteral(resourceName: "OpenBlockImg")
        }
        
        if let wordDictionary = Words.shared.wordsDictionary.words[dataCell.blockId], wordDictionary.blockWords.count > 0 {
            cell.countWordLabel.text = "Количество слов \(wordDictionary.blockWords.count)"
        } else {
            cell.countWordLabel.text = "Количество слов 0"
        }

        cell.blockNameLabel.isEnabled = dataCell.isOpen
        cell.descLabel.isEnabled = dataCell.isOpen
        cell.countWordLabel.isEnabled = dataCell.isOpen
        
        if !dataCell.isOpen {
            cell.selectionStyle = .none
            cell.backgroundColor = Setting.shared.colorIsOpenFalse
        } else {
            cell.selectionStyle = .default
            cell.backgroundColor = Setting.shared.colorIsOpenTrue
        }
        //cell.isUserInteractionEnabled = dataCell.isOpen   //можно или нет выбрать пункт таблицы
         
        return cell
    }
    
    //Выбрали уровень
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dataCell = Levels.shared.levelsArray.levels[indexPath.row]
        guard let wordDictionary = Words.shared.wordsDictionary.words[dataCell.blockId], wordDictionary.blockWords.count > 0 else {
            print("Error blockWords")
            return
        }
        
        if dataCell.isOpen {
            performSegue(withIdentifier: "MessageIsReadyViewController", sender: self)
        } else {
            let alertController = UIAlertController(title: "Внимание!", message: "чтобы открыть этот блок нужно пройти все предыдущие", preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(alertAction)
            present(alertController, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
 
}

