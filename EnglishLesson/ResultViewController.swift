//
//  ResultViewController.swift
//  EnglishLesson
//
//  Created by evgen on 19.03.19.
//  Copyright © 2019 evgen. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController {

    @IBOutlet weak var allTimeLabel: UILabel!
    @IBOutlet weak var allWordBlockLabel: UILabel!
    @IBOutlet weak var allGoodWordsLabel: UILabel!
    @IBOutlet weak var allBadWordsLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var victoryLabel: UILabel!
    @IBOutlet weak var nextBtt: UIButton!
    @IBOutlet weak var backListBtt: UIButton!
    
    var scores = 0
    var count = 0
    var seconds = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //параметри для кнопок
        nextBtt.layer.cornerRadius = 15  //округлення
        nextBtt.layer.shadowRadius = 5   //тінь
        nextBtt.layer.shadowOpacity = 0.5    //прозорість тіні
        nextBtt.layer.shadowOffset = CGSize(width: 0, height: 1.5)   //зсув тіні
        
        //параметри для кнопок
        backListBtt.layer.cornerRadius = 15  //округлення
        backListBtt.layer.shadowRadius = 5   //тінь
        backListBtt.layer.shadowOpacity = 0.5    //прозорість тіні
        backListBtt.layer.shadowOffset = CGSize(width: 0, height: 1.5)   //зсув тіні
        
        allTimeLabel.text = "\(seconds/60) минут \(seconds%60) секунд"
        allWordBlockLabel.text = "\(count) - всего слов в блоке"
        allGoodWordsLabel.text = "\(scores) - верных ответов"
        allBadWordsLabel.text = "\(count - scores) - не верных ответов"
        
        let valueSwitch = ((scores*100)/(count))
        
        switch (valueSwitch) {
        case 0...15:
            imageView.image = #imageLiteral(resourceName: "0_15_PercImg")
        case 16...30:
            imageView.image = #imageLiteral(resourceName: "16_30_PercImg")
        case 31...45:
            imageView.image = #imageLiteral(resourceName: "31_45_PercImg")
        case 46...60:
            imageView.image = #imageLiteral(resourceName: "46_60_PercImg")
        case 61...75:
            imageView.image = #imageLiteral(resourceName: "61_75_PercImg")
        case 76...90:
            imageView.image = #imageLiteral(resourceName: "76_90_PercImg")
        case 91...99:
            imageView.image = #imageLiteral(resourceName: "91_99_PercImg")
        case 100:
            imageView.image = #imageLiteral(resourceName: "100_PercImg")
            victoryLabel.isHidden = false
            Levels.shared.levelsArray.levels[levelsId].isDone = true
            Levels.shared.saveFileJSON()
            if levelsId < Levels.shared.levelsArray.levels.count-1 {
                levelsId += 1
                Levels.shared.levelsArray.levels[levelsId].isOpen = true
                Levels.shared.saveFileJSON()
                nextBtt.setTitle("Продолжить", for: .normal)
            } else {
                nextBtt.isHidden = true
            }
        default:
            imageView.image = nil
        }
        
        
    }
    
    
    
    @IBAction func nextBttAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func backListBttAction(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
}
