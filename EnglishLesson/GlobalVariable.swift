import Foundation
import UIKit

var levelsId: Int = 0  //Номер строки масива levels c которой сейчас работают

// Функція для читання файла типу json в текстову величину
func readFileJSON(fileName: String) -> String? {
    let dir = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] + "/" + fileName + ".json"
    let fileURL = URL(fileURLWithPath: dir)
    do {
        let text = try String(contentsOf: fileURL, encoding: .utf8)
        return text
    }
    catch {
        /* error handling here */
        return nil
    }
}

// Функція для запису у файл типу json з текстової величини
func writeFileJSON(fromText: String, fileName: String) -> Bool {
    let dir = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] + "/" + fileName + ".json"
    let fileURL = URL(fileURLWithPath: dir)
    do {
        try fromText.write(to: fileURL, atomically: false, encoding: .utf8)
        return true
    }
    catch {
        /* error handling here */
        return false
    }
}

//Функция для коппирования файлов в документы если их там нет
func copyFileToDocument(fileName: String) -> Bool {
    let dirDocument = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
    let toFile = dirDocument[0] + "/" + fileName + ".json"
    if !FileManager.default.fileExists(atPath: toFile) {
        let toFileURL = URL(fileURLWithPath: toFile)
        if let bundle = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do
            {
                try FileManager.default.copyItem(at: bundle, to: toFileURL)
                print("скопировал")
                return true
            }
            catch
            {
                print(error)
            }
        }
    } else {
        print("Файл уже существует")
    }
    return false
}

// ================ ПЕРВИЙ  JSON ======================
struct LevelsArray: Codable {
    var levels: [Level]
}

struct Level: Codable {
    let blockId: Int
    let blockName: String
    let desc: String
    var isOpen: Bool
    var isDone: Bool
}

class Levels: NSObject {
    static let shared = Levels()
    var levelsArray : LevelsArray = LevelsArray(levels: [Level.init(blockId: 1, blockName: "", desc: "", isOpen: false, isDone: false)])
    // Инициализация и чтение із файла
    override init() {
        if let textJSON = readFileJSON(fileName: "1") {
            let inputData =  textJSON.data(using: .utf8)!
            let decoder = JSONDecoder()
            levelsArray = try! decoder.decode(LevelsArray.self, from: inputData)
        }
    }
    //Запись у файл
    func saveFileJSON() {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let data = try! encoder.encode(levelsArray)
        let json = String(data: data, encoding: .utf8) ?? ""
        if !writeFileJSON(fromText: json, fileName: "1") {
            print("Error write File 1.json")
        }
    }
}


// ================ ВТОРОЙ  JSON ======================

struct WordsDictionary: Codable {
    let words: [Int:WordsStr]
}

struct WordsStr: Codable {
    var blockWords: [String:String]
    var secForOneWord: Int
}

class Words: NSObject {
    static let shared = Words()
    var wordsDictionary : WordsDictionary = WordsDictionary(words: [0:WordsStr.init(blockWords: ["":""], secForOneWord: 0)])
    // Инициализация и чтение із файла
    override init() {
        if let textJSON = readFileJSON(fileName: "2") {
            let inputData =  textJSON.data(using: .utf8)!
            let decoder = JSONDecoder()
            do {
                wordsDictionary = try decoder.decode(WordsDictionary.self, from: inputData)
            }
            catch {
                print("Ошибка декодирования файла 2.json")
            }
        }
    }
}


