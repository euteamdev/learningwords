//
//  WordsViewController.swift
//  EnglishLesson
//
//  Created by evgen on 16.03.19.
//  Copyright © 2019 evgen. All rights reserved.
//

import UIKit

class WordsViewController: UIViewController {
    
    //MARK: - Variable
    var blockWords : [String:String] = [:]
    var secForOneWord = 0
    var englishWord : [String] = [] //Массив ключей-англ.слов
    var randomInt = 0   //для генерации случайного числа
    var timerCount = 0  //Счетчик для таймера
    var scores = 0      //Счетчик балов
    var timer: Timer?
    
    //MARK: - Outlet
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var scoresLabel: UILabel!
    @IBOutlet weak var englishWordLabel: UILabel!
    @IBOutlet weak var russianWordLabel: UITextField!
    @IBOutlet weak var goodWordLabel: UILabel!
    @IBOutlet weak var isReadyBtt: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //параметри для кнопок
        isReadyBtt.layer.cornerRadius = 15  //округлення
        isReadyBtt.layer.shadowRadius = 5   //тінь
        isReadyBtt.layer.shadowOpacity = 0.5    //прозорість тіні
        isReadyBtt.layer.shadowOffset = CGSize(width: 0, height: 1.5)   //зсув тіні
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        start()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {    //  підготовка до переходу
        guard let controller = segue.destination as? ResultViewController else {fatalError("error")}
        controller.seconds = blockWords.count * secForOneWord - timerCount
        controller.count = blockWords.count
        controller.scores = scores
    }
    
    //MARK: - Action
    @IBAction func readyBttAction(_ sender: Any) {
        readyBtt()
    }

    @IBAction func backBttAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    //MARK: - Function
    func start() {
        timerLabel.text = ""
        scoresLabel.text = "0"
        let level = Levels.shared.levelsArray.levels[levelsId]
        let blockId = level.blockId
        let wordDictionary = (Words.shared.wordsDictionary.words[blockId])!
        blockWords = wordDictionary.blockWords
        secForOneWord = wordDictionary.secForOneWord
        scores = 0
        englishWord = []
        for item in blockWords.keys {
            englishWord.append(item)
        }
        //устанавл таймер
        timerCount = blockWords.count * secForOneWord
        startTimer()
        setupWord()
    }
    
    //Старт таймера
    func startTimer(){
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (timer) in
            self.timerCount -= 1
            self.timerLabel.text = "\(self.timerCount)"
            if self.timerCount <= 0 {
                self.finish()
            }
        }
    }
    
    //Выбор рандомного слова из массива
    func setupWord() {
        randomInt = Int(arc4random_uniform(UInt32(englishWord.count)))
        englishWordLabel.text = englishWord[randomInt]
        russianWordLabel.becomeFirstResponder()
    }

    //Когда ответ готов
    func readyBtt() {
        isReadyBtt.isEnabled = false
        if russianWordLabel.text?.trimmingCharacters(in: .whitespaces).uppercased() == blockWords[englishWord[randomInt]]?.trimmingCharacters(in: .whitespaces).uppercased() {
            scores += 1
            scoresLabel.text = "\(scores)"
            englishWordLabel.textColor = Setting.shared.colorGoodAnswer
            russianWordLabel.textColor = Setting.shared.colorGoodAnswer
        } else {
            englishWordLabel.textColor = Setting.shared.colorBadAnswer
            russianWordLabel.textColor = Setting.shared.colorBadAnswer
            goodWordLabel.text = blockWords[englishWord[randomInt]]
        }
        timer?.invalidate() //Остановка основного таймера
        //Запуск дополнительного таймера
        Timer.scheduledTimer(withTimeInterval: TimeInterval(Setting.shared.timePause), repeats: false) { (timer) in
            //После окончания дополнителього таймера
            self.goodWordLabel.text = ""
            self.russianWordLabel.text = ""
            self.englishWordLabel.textColor = Setting.shared.colorDefaultAnswer
            self.russianWordLabel.textColor = Setting.shared.colorDefaultAnswer
            self.englishWord.remove(at: self.randomInt)
            self.isReadyBtt.isEnabled = true
            self.startTimer()
            if self.englishWord.count > 0 {
                self.setupWord()
            } else {
                self.finish()
            }
        }
    }
    
    //Когда закончилось время или слова
    func finish() {
        timer?.invalidate()
        russianWordLabel.resignFirstResponder()
        performSegue(withIdentifier: "ResultViewController", sender: self)
    }


}

//MARK: - extension
extension WordsViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        readyBtt()
        return true
    }
}
