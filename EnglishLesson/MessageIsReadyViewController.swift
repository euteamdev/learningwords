//
//  MessageIsReadyViewController.swift
//  EnglishLesson
//
//  Created by evgen on 18.03.19.
//  Copyright © 2019 evgen. All rights reserved.
//

import UIKit

class MessageIsReadyViewController: UIViewController {

    var blockWords : [String:String] = [:]
    var desc = ""
    
    @IBOutlet weak var countWordLabel: UILabel!
    @IBOutlet weak var isNotReadyBtt: UIButton!
    @IBOutlet weak var isReadyBtt: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //параметри для кнопок
        isReadyBtt.layer.cornerRadius = 15  //округлення
        isReadyBtt.layer.shadowRadius = 5   //тінь
        isReadyBtt.layer.shadowOpacity = 0.5    //прозорість тіні
        isReadyBtt.layer.shadowOffset = CGSize(width: 0, height: 1.5)   //зсув тіні
        
        //параметри для кнопок
        isNotReadyBtt.layer.cornerRadius = 15  //округлення
        isNotReadyBtt.layer.shadowRadius = 5   //тінь
        isNotReadyBtt.layer.shadowOpacity = 0.5    //прозорість тіні
        isNotReadyBtt.layer.shadowOffset = CGSize(width: 0, height: 1.5)   //зсув тіні
        
        let level = Levels.shared.levelsArray.levels[levelsId]
        //Levels.shared.levelsArray[levelsId]
        let blockId = level.blockId
        let wordDictionary = (Words.shared.wordsDictionary.words[blockId])!
        blockWords = wordDictionary.blockWords
        desc = level.desc
        
        countWordLabel.text = "Количество слов \(blockWords.count)"
        descriptionLabel.text = desc
    }
    @IBAction func isNotReadyBttAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
}
