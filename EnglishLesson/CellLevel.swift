//
//  CellLevel.swift
//  EnglishLesson
//
//  Created by evgen on 16.03.19.
//  Copyright © 2019 evgen. All rights reserved.
//

import UIKit

class CellLevel: UITableViewCell {

    @IBOutlet weak var blockNameLabel: UILabel!
    @IBOutlet weak var imageViewLb: UIImageView!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var countWordLabel: UILabel!
    
    var isDone = false
    var isOpen = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
