//
//  Setting.swift
//  EnglishLesson
//
//  Created by evgen on 17.03.19.
//  Copyright © 2019 evgen. All rights reserved.
//

import Foundation
import UIKit

class Setting {
    
    static let shared = Setting()
    let timePause = 3           //пауза для просмотра верно или нет
    let colorGoodAnswer = #colorLiteral(red: 0, green: 0.9768045545, blue: 0, alpha: 1)     // цвет правильного ответа
    let colorBadAnswer = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)      //цвет не правильного ответа
    let colorDefaultAnswer = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)     // цвет по умолчанию
    let textLabelOverTable = "Блоки ниже это уровни,\nвыберай и проходи"  //Текст над таблицей
    let colorLabelOverTable = #colorLiteral(red: 0.8039215686, green: 0.8511025746, blue: 0.9961118102, alpha: 1)  //Цвет для фона надписи над таблицей
    let colorIsOpenTrue = #colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1)    //Цвет текста в блоках на первом экране если IsOpen true
    let colorIsOpenFalse = #colorLiteral(red: 0.9098039269, green: 0.7635108497, blue: 0.8551112653, alpha: 1)    //Цвет текста в блоках на первом экране если IsOpen false
}
